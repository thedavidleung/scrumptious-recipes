# Generated by Django 4.1 on 2022-08-30 14:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("recipes", "0006_alter_ingredient_measure"),
    ]

    operations = [
        migrations.AlterField(
            model_name="measure",
            name="abbreviation",
            field=models.CharField(blank=True, max_length=30, unique=True),
        ),
        migrations.AlterField(
            model_name="measure",
            name="name",
            field=models.CharField(blank=True, max_length=30, unique=True),
        ),
    ]
